# chirp/views.py

from django.shortcuts import render
from .models import Profile
from django.contrib.auth.decorators import login_required

def dashboard(request):
    return render(request, "base.html")

@login_required
def profile_list(request):
    profiles = Profile.objects.exclude(user=request.user)
    return render(request, "chirp/profile_list.html", {"profiles": profiles})

def profile(request, pk):
    profile = Profile.objects.get(pk=pk)
    return render(request, "chirp/profile.html", {"profile": profile})
